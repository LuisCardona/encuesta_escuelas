<?php
  //error_reporting(E_ERROR);
 ini_set('defaul_charset', "UTF-8");
  include("template/header.php");
  include("template/head.php");

?>
  <!-- PAGE CONTENT BEGIN -->
    <div class="container-fluid">
      <div class="page-content page-content-popup">
        <!-- BEGIN PAGE CONTENT FIXED -->
      <div class="page-content-fixed-header">
        <div class="content-header-menu">
            <!-- BEGIN MENU TOGGLER -->
            <button type="button" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                      <span class="toggle-icon">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </span>
                  </button>
            <!-- END MENU TOGGLER -->
          </div>
      </div>

      <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
          <!-- BEGIN SIDEBAR MENU -->
         
                <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                  <li id="menu_registro">
                    <a href="javascript:;">
                      <i class="fa fa-child"></i>
                      <span class="title">Contestar Encuesta</span>
                    </a>
                  </li>
                  <li id="menu_reporte">
                    <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i>
                      <span class="title">Consulta de Encuestas</span>
                    </a>
                  </li>                  
                </ul>
            
      </div>
    </div>

      <div id="principal" class="page-fixed-main-content">
        <?php
        include ("_system/frm_principal.php");
        ?>
      </div>
      <!-- BEGIN SIDEBAR -->

    </div>
  </div>


  <?php
  include ("template/footer.php");
  ?>
