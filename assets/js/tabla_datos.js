var tabla_datos = function() {

    var table = $('.tabla_busqueda_paginado');

                            /*Coloca los botones del tabletools en un container*/
                            /*$.extend(true, $.fn.DataTable.TableTools.classes, {
                                "container": "btn-group tabletools-btn-group pull-right",
                                "buttons": {
                                    "normal": "btn btn-sm yellow default",
                                    "disabled": "btn btn-sm yellow default disabled"
                                }
                            });*/

                        var oTable = table.dataTable({
                            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No hay registros disponibles en la tabla",
                "info": "_START_ de _END_ de _TOTAL_ registros",
                "infoEmpty": "Registros no encontrados",
                "infoFiltered": "(Filtrado de _MAX_ registros)",
                "lengthMenu": "_MENU_ Registros",
                "search": "Búsqueda:",
                "zeroRecords": "No se encontraron registros",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [
                { extend: 'print', className: 'btn btn-danger', text: 'Imprimir' },
                { extend: 'copy', className: 'btn btn-danger', text: 'Copiar' },
                { extend: 'pdf', className: 'btn btn-danger', text: 'Exportar PDF' },
                { extend: 'excel', className: 'btn btn-danger', text: 'Exportar Excel' },
                { extend: 'csv', className: 'btn btn-danger', text: 'Exportar CSV' }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,

            "dom": "<'row margin-bottom-30'<'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
                        });
            oTable.$("[data-toggle='popover']").popover({trigger: 'hover','placement': 'top'});
};
