<?php 
include '../class/class_encuesta_dal.php';
$obj = new encuesta_dal();

$folio = $_POST['folio'];
$datos = $obj->get_by_folio($folio);

$lista = null;
$i=0;
if (sizeof($datos) > 0) {
	foreach ($datos as $key => $value) {
		$data = array(
			'folio' => $value->getfolio(),
            'cct' => $value->getcct(),
            'turno' => $value->getturno(),
            'nombre' => $value->getnombre(),
            'municipio' => $value->getmunicipio(),
            'nivel' => $value->getnivel(),
            'zona_escolar' => $value->getzona_escolar(),
            'jefatura' => $value->getjefatura(),
            'vandalizado' => $value->getvandalizado(),
            'vandalizado_si' => $value->getvandalizado_si(),
            'atendido' => $value->getatendido(),
            'servicio_electrico' => $value->getservicio_electrico(),
            'focos' => $value->getfocos(),
            'contactos' => $value->getcontactos(),
            'red_electrica_interior' => $value->getred_electrica_interior(),
            'red_electrica_exterior' => $value->getred_electrica_exterior(),
            'servicio_agua' => $value->getservicio_agua(),
            'red_agua' => $value->getred_agua(),
            'servicio_drenaje' => $value->getservicio_drenaje(),
            'red_drenaje' => $value->getdrenaje(),
            'fosa_septica' => $value->getfosa_septica(),
            'inodoro' => $value->getinodoro(),
            'mingitorios' => $value->getmingitorios(),
            'lavamanos' => $value->getlavamanos(),
            'puertas' => $value->getpuertas(),
            'chapas' => $value->getchapas(),
            'vidrios' => $value->getvidrios(),
            'protecciones' => $value->getprotecciones(),
            'maleza' => $value->getmaleza(),
            'basura' => $value->getbasura(),
            'escombro' => $value->getescombro(),
		);
		$lista["aaData"][$i]=$data;
		unset($data);
		$i++;
	}
}else{
	$lista["aaData"] = [];
}

$json = json_encode($lista);
unset($datos);
header('Content-type: application/json; charset=utf-8');
echo $json;
unset($json);
exit();
?>