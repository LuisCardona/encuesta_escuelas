<?php
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    session_start();

    function replace($cadena){
        $caracteres = array("'", ",", "\\" , "/", "#","@","$","&","*","?","%",'"',"=",".");
        $cadena = str_replace( $caracteres, "", $cadena );
        return $cadena;
    }    

    if(isset($_POST["usr"])){
        $usr = replace($_POST['usr']);
    }else{
        echo "falta el parametro usr";
    }

    if(isset($_POST["psw"])){
        $psw = replace($_POST['psw']);
    }else{
        echo "falta el parametro psw";
    }
	
    //verificamos si existe ese usuario y ese password
    //traemos los datos por usuario y password
    //los asignamos a las variables de sesion y se retorna una respuesta
    include '../class/class_usuarios_dal.php';
    $obj = new usuarios_dal();
    $existe = $obj->existe($usr,$psw);
    
    if($existe >= 1){
        $resultado = $obj->get_region($usr,$psw);

        $_SESSION["usuario"]=$usr;
        $_SESSION['psw'] = $psw;
        $_SESSION['cve_region'] = $resultado->getcve_region();
     
        $salida["procede"]="1";
		$salida["status"]="1";
		$salida["statusText"]="usuario correcto";
		echo json_encode($salida);
		return;
    }else{
        $salida["procede"]="0";
		$salida["status"]="0";
		$salida["statusText"]="no existe usuario";
		echo json_encode($salida);
		return;
    }
    
?>