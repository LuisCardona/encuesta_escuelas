<?php 
include '../class/class_encuesta_dal.php';
$obj = new encuesta_dal();
$cct = $_GET["cct"];
$datos = $obj->get_datos_cct2($cct);

$lista = null;

if ($datos > 0) {
		$data = array(
        'nombre' => $datos['nombre'],   
        'turno' => $datos['turno'],   
		'desc_turno' => $datos['desc_turno'],
        'domicilio' => $datos['domicilio'],
        'nombre_de_municipio' => $datos['nombre_de_municipio'],  
         'zona_escolar' => $datos['zona_escolar'], 
          'jefatura_de_sector' => $datos['jefatura_de_sector'],  
           'nivel' => $datos['nivel'],     
             'region' => $datos['region'],                	
		);
		$lista=$data;
		unset($data);
}else{
	$lista["aaData"] = [];
}


$json = json_encode($lista);
unset($datos);
header('Content-type: application/json; charset=utf-8');
echo $json;
unset($json);
exit();
?>