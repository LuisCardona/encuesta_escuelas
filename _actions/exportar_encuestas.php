<?php
header ( "Content-type: application/vnd.ms-excel" );
header ( "Content-Disposition: attachment; filename=encuesta_escuelas.xls" );
echo "<table>";
echo "<tr>
      <th>Folio</th>
      <th>CCT</th>
      <th>Turno</th>
      <th>Nombre</th>
      <th>Municipio</th>
      <th>Localidad</th>
      <th>Region</th>
      <th>Nivel</th>
      <th>Zona escolar</th>
      <th>Jefatura</th>
      <th>¿vandalizado?</th>
      <th>Descripcion vandalizado</th>
      <th>¿Fue atendido?</th>
      <th>Servicio electrico</th>
      <th>Focos</th>
      <th>Contactos</th>
      <th>Red electrica interior</th>
      <th>Red electrica exterior</th>
      <th>Servicio agua</th>
      <th>Red agua</th>
      <th>Servicio drenaje</th>
      <th>Red drenaje</th>
      <th>Fosa septica</th>
      <th>Inodoro</th>
      <th>Mingitorios</th>
      <th>Lavamanos</th>
      <th>Puertas</th>
      <th>Chapas</th>
      <th>Vidrios</th>
      <th>Protecciones</th>
      <th>Maleza</th>
      <th>Basura</th>
      <th>Escombro</th>
      </tr>";


include '../class/class_encuesta_dal.php';
$obj = new encuesta_dal();

$datos = $obj->get_all();

$lista = null;
$i=0;
if (sizeof($datos) > 0) {
	foreach ($datos as $key => $value) {
			$data = array(
			'folio' => $value->getfolio(),
            'cct' => utf8_encode($value->getcct()),
            'turno' => $value->getturno(),
            'nombre' => utf8_encode($value->getnombre()),
            'municipio' => utf8_encode($value->getmunicipio()),
            'localidad' => utf8_encode($value->getlocalidad()),
            'region' => $value->getregion(),
            'nivel' => $value->getnivel(),
            'zona_escolar' => $value->getzona_escolar(),
            'jefatura' => $value->getjefatura(),
            'vandalizado' => $value->getvandalizado(),
            'vandalizado_si' => utf8_encode($value->getvandalizado_si()),
             'atendido' => utf8_encode($value->getatendido()),
            'servicio_electrico' => $value->getservicio_electrico(),
            'focos' => $value->getfocos(),
            'contactos' => $value->getcontactos(),
            'red_electrica_interior' => $value->getred_electrica_interior(),
            'red_electrica_exterior' => $value->getred_electrica_exterior(),
            'servicio_agua' => $value->getservicio_agua(),
            'red_agua' => $value->getred_agua(),
            'servicio_drenaje' => $value->getservicio_drenaje(),
            'red_drenaje' => $value->getred_drenaje(),
            'fosa_septica' => $value->getfosa_septica(),
            'inodoro' => $value->getinodoro(),
            'mingitorios' => $value->getmingitorios(),
            'lavamanos' => $value->getlavamanos(),
            'puertas' => $value->getpuertas(),
            'chapas' => $value->getchapas(),
            'vidrios' => $value->getvidrios(),
            'protecciones' => $value->getprotecciones(),
            'maleza' => $value->getmaleza(),
            'basura' => $value->getbasura(),
            'escombro' => $value->getescombro(),
		);
		$lista["aaData"][$i]=$data;
		unset($data);
		$i++;
		echo "<tr>
      <th>".$value->getfolio()."</th>
      <th>".$value->getcct()."</th>
      <th>".$value->getturno()."</th>
      <th>".$value->getnombre()."</th>
      <th>".$value->getmunicipio()."</th>
      <th>".$value->getlocalidad()."</th>
      <th>".$value->getregion()."</th>
      <th>".$value->getnivel()."</th>
      <th>".$value->getzona_escolar()."</th>
      <th>".$value->getjefatura()."</th>
      <th>".$value->getvandalizado()."</th>
      <th>".$value->getvandalizado_si()."</th>
      <th>".$value->getatendido()."</th>
      <th>".$value->getservicio_electrico()."</th>
      <th>".$value->getfocos()."</th>
      <th>".$value->getcontactos()."</th>
      <th>".$value->getred_electrica_interior()."</th>
      <th>".$value->getred_electrica_exterior()."</th>
      <th>".$value->getservicio_agua()."</th>
      <th>".$value->getred_agua()."</th>
      <th>".$value->getservicio_drenaje()."</th>
      <th>".$value->getred_drenaje()."</th>
      <th>".$value->getfosa_septica()."</th>
      <th>".$value->getinodoro()."</th>
      <th>".$value->getmingitorios()."</th>
      <th>".$value->getlavamanos()."</th>
      <th>".$value->getpuertas()."</th>
      <th>".$value->getchapas()."</th>
      <th>".$value->getvidrios()."</th>
      <th>".$value->getprotecciones()."</th>
      <th>".$value->getmaleza()."</th>
      <th>".$value->getbasura()."</th>
       <th>".$value->getescombro()."</th>
      </tr>";

	}
}else{
	$lista["aaData"] = [];
}

?>