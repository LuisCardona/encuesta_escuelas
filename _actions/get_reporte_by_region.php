<?php 
include '../class/class_encuesta_dal.php';
$obj = new encuesta_dal();

$region=$_POST['region'];
$datos = $obj->get_by_region_original($region);

$lista = null;
$i=0;
if ($datos!=NULL) {
	foreach ($datos as $key => $value) {
		$data = array(
			'folio' => $value->getfolio(),
            'cct' => utf8_encode($value->getcct()),
            'turno' => $value->getturno(),
            'nombre' => utf8_encode($value->getnombre()),
            'municipio' => utf8_encode($value->getmunicipio()),
            'localidad' => utf8_encode($value->getlocalidad()),
            'region' => $value->getregion(),
            'nivel' => $value->getnivel(),
            'zona_escolar' => $value->getzona_escolar(),
            'jefatura' => $value->getjefatura(),
            'vandalizado' => $value->getvandalizado(),
            'vandalizado_si' => utf8_encode($value->getvandalizado_si()),
             'atendido' => utf8_encode($value->getatendido()),
            'servicio_electrico' => $value->getservicio_electrico(),
            'focos' => $value->getfocos(),
            'contactos' => $value->getcontactos(),
            'red_electrica_interior' => $value->getred_electrica_interior(),
            'red_electrica_exterior' => $value->getred_electrica_exterior(),
            'servicio_agua' => $value->getservicio_agua(),
            'red_agua' => $value->getred_agua(),
            'servicio_drenaje' => $value->getservicio_drenaje(),
            'red_drenaje' => $value->getred_drenaje(),
            'fosa_septica' => $value->getfosa_septica(),
            'inodoro' => $value->getinodoro(),
            'mingitorios' => $value->getmingitorios(),
            'lavamanos' => $value->getlavamanos(),
            'puertas' => $value->getpuertas(),
            'chapas' => $value->getchapas(),
            'vidrios' => $value->getvidrios(),
            'protecciones' => $value->getprotecciones(),
            'maleza' => $value->getmaleza(),
            'basura' => $value->getbasura(),
            'escombro' => $value->getescombro(),
		);
		$lista["aaData"][$i]=$data;
		unset($data);
		$i++;
	}
}else{
	$lista["aaData"] = [];
}

$json = json_encode($lista);
unset($datos);
header('Content-type: application/json; charset=utf-8');
echo $json;
unset($json);
exit();
?>