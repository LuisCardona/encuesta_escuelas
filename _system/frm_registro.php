<?php

ini_set('defaul_charset', "UTF-8");
session_start();
include_once '../class/class_encuesta_dal.php';
$encuesta_dal=new encuesta_dal();
$escuelas=$encuesta_dal->get_datos_cct();

?>
<meta charset="UTF-8">
<div class="portlet light bordered col-md-8">
                                <div class="portlet-title">
                                    <div class="caption font-green-haze">
                                        <i class="icon-settings font-green-haze" ></i>
                                        <span  class="caption-subject bold uppercase"> Encuesta Escolar de Servicios</span>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form role="form" class="form-horizontal"  id="nueva_encuesta" name="nueva_encuesta" method="post" action="" autocomplete="on" enctype="multipart/form-data">
                                        <div class="form-body">
                                         <input type="hidden" class="form-control" id="region" name="region">
                                        <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">CCT</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                       <select id="cct" name="cct" class="form-control" type="text" required  onchange="javascript:showContent()">
                                                       <option value="">SELECCIONE SU ESCUELA:</option>
                                                       <option value="05DPR0467W">05DPR0467W - GNRL. FRANCISCO VILLA - TURNO VESPERTINO - TORREÓN</option>
									<?php 
									foreach ($escuelas as $key => $value) {
									?>
										<option value="<?=$value['cct']?>"><?=$value['cct']." - ".$value['nombre']." - ".$value['desc_turno']." - ".$value['domicilio']." - ".$value['nombre_de_municipio']?></option>
									<?php
									} 
									unset($escuelas);
									?>
									
									</select>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Turno</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="turno" name="turno">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa el Turno de la Escuela</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Nombre</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="nombre" name="nombre">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa el Nombre de la Escuela</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Municipio</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="municipio" name="municipio">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa el Municipio</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Localidad</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="localidad" name="localidad">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa la Localidad</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Nivel</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="nivel" name="nivel">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa el Nivel Educativo</span>
                                                       
                                                    </div>
                                                </div>
                                            </div> <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Zona Escolar</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="zona_escolar" name="zona_escolar">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa la Zona Escolar</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Jefatura</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="jefatura" name="jefatura">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa la Jefatura</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>

										 <label style="text-align: center;"><b>PLANTEL VANDALIZADO</b></label><br><br>
									   <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">¿El plantel ha sido vandalizado?</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="checkbox1" name="checkbox1" class="md-radiobtn" value="SI">
                                                            <label for="checkbox1">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>SI</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="checkbox2" name="checkbox1" class="md-radiobtn" value="NO">
                                                            <label for="checkbox2">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>NO</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">En caso de ser, SI. Especifique: </label>
                                                <div class="col-md-10">
                                                    <textarea class="form-control" rows="3" placeholder="Especifique" id="vandalizado_si" name="vandalizado_si"></textarea>
                                                    <div class="form-control-focus"> </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">En caso de que la respuesta anterior sea afirmativa, Indique cuales daños han sido atendidos: </label>
                                                <div class="col-md-10">
                                                    <textarea class="form-control" rows="3" placeholder="Especifique" id="atendido" name="atendido"></textarea>
                                                    <div class="form-control-focus"> </div>
                                                </div>
                                            </div>
											 <label class="col-md-6.5 control-label" for="form_control_1"><b>SISTEMA ELÉCTRICO E ILUMINACIÓN</b></label><br><br>
											 <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Actualmente, ¿Cuenta con servicio de Energia Eléctrica?</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="checkbox3" name="checkbox2" class="md-radiobtn" value="SI">
                                                            <label for="checkbox3">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>SI</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="checkbox4" name="checkbox2" class="md-radiobtn" value="NO">
                                                            <label for="checkbox4">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>NO</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Focos o Lamparas</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio1" name="radio1" class="md-radiobtn" value="Bueno">
                                                            <label for="radio1">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio2" name="radio1" class="md-radiobtn" value="Regular">
                                                            <label for="radio2">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio3" name="radio1" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio3">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Contactos</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio4" name="radio2" class="md-radiobtn" value="Bueno">
                                                            <label for="radio4">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio5" name="radio2" class="md-radiobtn" value="Regular">
                                                            <label for="radio5">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio6" name="radio2" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio6">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Red Eléctrica Interior (Aulas)</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio7" name="radio3" class="md-radiobtn" value="Bueno">
                                                            <label for="radio7">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio8" name="radio3" class="md-radiobtn" value="Regular">
                                                            <label for="radio8">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio9" name="radio3" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio9">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Red Eléctrica Exterior (Registros)</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio10" name="radio4" class="md-radiobtn" value="Bueno">
                                                            <label for="radio10">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio11" name="radio4" class="md-radiobtn" value="Regular">
                                                            <label for="radio11">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio12" name="radio4" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio12">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											 <label class="col-md-6.5 control-label" for="form_control_1"><b>SISTEMA DE AGUA POTABLE</b></label><br><br>
											 <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Actualmente, ¿Cuenta con servicio de Agua Potable?</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="checkbox5" name="checkbox3" class="md-radiobtn" value="SI">
                                                            <label for="checkbox5">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>SI</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="checkbox6" name="checkbox3" class="md-radiobtn" value="NO">
                                                            <label for="checkbox6">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>NO</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Red de Agua Potable</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio13" name="radio5" class="md-radiobtn" value="Bueno">
                                                            <label for="radio13">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio14" name="radio5" class="md-radiobtn" value="Regular">
                                                            <label for="radio14">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio15" name="radio5" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio15">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<label class="col-md-6.5 control-label" for="form_control_1"><b>SISTEMA DE DRENAJE</b></label><br><br>
                                             <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Actualmente, ¿Cuenta con servicio de Drenaje?</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="checkbox7" name="checkbox4" class="md-radiobtn" value="SI">
                                                            <label for="checkbox7">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>SI</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="checkbox8" name="checkbox4" class="md-radiobtn" value="NO">
                                                            <label for="checkbox8">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>NO</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Red de Drenaje Externa (Registros)</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio16" name="radio6" class="md-radiobtn" value="Bueno">
                                                            <label for="radio16">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio17" name="radio6" class="md-radiobtn" value="Regular">
                                                            <label for="radio17">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio18" name="radio6" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio18">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input">
                                             <label class="col-md-10 control-label" for="form_control_1">*Si cuenta con Servicio de Drenaje en "Fosa Séptica" asignele "Sin Funcionar"*</label>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Fosa Séptica</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio19" name="radio7" class="md-radiobtn" value="Bueno">
                                                            <label for="radio19">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio20" name="radio7" class="md-radiobtn" value="Regular">
                                                            <label for="radio20">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio21" name="radio7" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio21">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<label class="col-md-6.5 control-label" for="form_control_1"><b>SERVICIOS DE SANITARIOS</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Inodoro</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio22" name="radio8" class="md-radiobtn" value="Bueno">
                                                            <label for="radio22">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio23" name="radio8" class="md-radiobtn" value="Regular">
                                                            <label for="radio23">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio24" name="radio8" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio24">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Mingitorios</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio25" name="radio9" class="md-radiobtn" value="Bueno">
                                                            <label for="radio25">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio26" name="radio9" class="md-radiobtn" value="Regular">
                                                            <label for="radio26">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio27" name="radio9" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio27">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Lavamanos</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio28" name="radio10" class="md-radiobtn" value="Bueno">
                                                            <label for="radio28">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio29" name="radio10" class="md-radiobtn" value="Regular">
                                                            <label for="radio29">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio30" name="radio10" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio30">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<label class="col-md-6.5 control-label" for="form_control_1"><b>PUERTAS Y CANCELERÍA</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Puertas</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio31" name="radio11" class="md-radiobtn" value="Bueno">
                                                            <label for="radio31">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio32" name="radio11" class="md-radiobtn" value="Regular">
                                                            <label for="radio32">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio33" name="radio11" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio33">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Chapas</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio34" name="radio12" class="md-radiobtn" value="Bueno">
                                                            <label for="radio34">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio35" name="radio12" class="md-radiobtn" value="Regular">
                                                            <label for="radio35">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio36" name="radio12" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio36">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Vidrios</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio37" name="radio13" class="md-radiobtn" value="Bueno">
                                                            <label for="radio37">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio38" name="radio13" class="md-radiobtn" value="Regular">
                                                            <label for="radio38">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio39" name="radio13" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio39">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Protecciones</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio40" name="radio14" class="md-radiobtn" value="Bueno">
                                                            <label for="radio40">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio41" name="radio14" class="md-radiobtn" value="Regular">
                                                            <label for="radio41">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio42" name="radio14" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio42">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<label class="col-md-6.5 control-label" for="form_control_1"><b>HIGIENE Y LIMPIEZA</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Maleza</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio43" name="radio15" class="md-radiobtn" value="Mucha">
                                                            <label for="radio43">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Mucha</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio44" name="radio15" class="md-radiobtn" value="Poca">
                                                            <label for="radio44">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Poca</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio45" name="radio15" class="md-radiobtn" value="Nada">
                                                            <label for="radio45">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Nada</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Basura</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio46" name="radio16" class="md-radiobtn" value="Mucha">
                                                            <label for="radio46">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Mucha</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio47" name="radio16" class="md-radiobtn" value="Poca">
                                                            <label for="radio47">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Poca</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio48" name="radio16" class="md-radiobtn" value="Nada">
                                                            <label for="radio48">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Nada</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Escombro</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio49" name="radio17" class="md-radiobtn" value="Mucha">
                                                            <label for="radio49">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Mucha</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio50" name="radio17" class="md-radiobtn" value="Poca">
                                                            <label for="radio50">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Poca</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio51" name="radio17" class="md-radiobtn" value="Nada">
                                                            <label for="radio51">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Nada</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-5 col-md-2">
                                                    <button type="button" class="btn blue" id="btnGuardar" name="btnGuardar">Enviar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

<script>

 $('#cct').select2({
          width:'100%',
          dropdownParent: $('#nueva_encuesta')
          });

function showContent(){
    $.ajax({                        
       	type: "GET",                 
        url: '_actions/get_datos_cct.php',                    
        data: {
            cct: $("#cct").val()
            },
       	success: function(resultado)            
       {
		 console.log(resultado); 
          $('#nombre').val(resultado.nombre);
          $('#turno').val(resultado.desc_turno);
          $('#municipio').val(resultado.nombre_de_municipio);  
          $('#localidad').val(resultado.nombre_de_municipio); 
         $('#zona_escolar').val(resultado.zona_escolar);   
          $('#jefatura_de_sector').val(resultado.jefatura_de_sector);
           $('#nivel').val(resultado.nivel);
             $('#region').val(resultado.region);
       }
     });
}
	//FUNCION PARA GUARDAR REGISTRO DEL ALUMNO
		 $('#btnGuardar').click(function (e) {
            var radio1 = $("input[name='radio1']:checked").length;
            var radio2 = $("input[name='radio2']:checked").length;
            var radio3 = $("input[name='radio3']:checked").length;
            var radio4 = $("input[name='radio14']:checked").length;
            var radio5 = $("input[name='radio5']:checked").length;
            var radio6 = $("input[name='radio6']:checked").length;
             var radio7 = $("input[name='radio6']:checked").length;
            var radio8 = $("input[name='radio8']:checked").length;
            var radio9 = $("input[name='radio9']:checked").length;
            var radio10 = $("input[name='radio10']:checked").length;
            var radio11 = $("input[name='radio11']:checked").length;
            var radio12 = $("input[name='radio12']:checked").length;
            var radio13 = $("input[name='radio13']:checked").length;
            var radio14 = $("input[name='radio14']:checked").length;
            var radio15 = $("input[name='radio15']:checked").length;
            var radio16 = $("input[name='radio16']:checked").length;
            var radio17 = $("input[name='radio17']:checked").length;
             if(radio1=="" || radio2=="" || radio3=="" || radio4=="" || radio5=="" || radio6=="" || radio8=="" || radio9=="" || radio10=="" || radio11==""
            || radio12=="" || radio13=="" || radio14=="" || radio15=="" || radio16=="" || radio17=="" || radio7==""){
                 alert("Faltan preguntas por contestar");
             }else{
                    var formData = new FormData(document.getElementById("nueva_encuesta"));
							$.ajax({
                            url: '_actions/insert_encuesta.php',
                            type: 'POST',
                            dataType: 'html',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (resultado) {
                                console.log(resultado);
                                   alert(resultado);
									document.getElementById("nueva_encuesta").reset();
                            },
                            error: function (xhr, desc, err) {
                                console.log(xhr);
                                console.log("Details:" + desc + "\nError" + err);
                            }
                        });
                    }
            });
     //TERMINA LA FUNCION PARA GUARDAR REGISTRO DEL ALUMNO


</script>