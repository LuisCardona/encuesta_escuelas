<?php
ini_set('defaul_charset', "UTF-8");
session_start();

if(isset($_SESSION["usuario"])){
    $usuario = $_SESSION["usuario"];
    $cve_region=$_SESSION["cve_region"];
} else {
  ?>
  <script>
    window.location = '_system/frm_login.php';
  </script>
  <?php
}   

include_once '../class/class_encuesta_dal.php';
$encuesta_dal=new encuesta_dal();
$region = $_SESSION['cve_region'];
$escuelas_faltantes=$encuesta_dal->get_escuelas_faltantes($region);

?>

<meta hhtp-equiv="Content-Type" content="text/html; charset=utf-8">
<div class="row">
    <div class="col-md-10">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Lista de Encuestas por Region
                </div>
            </div>
            <div class="portlet-body flip-scroll">
            <form id="buscar_asunto">
  		  <div class="form-group row">
  	    	<div class="col-sm-8">
  		    	<select class="form-control" id="region_enviar" name="region_enviar" readonly="">
                  <option value="<?=$_SESSION["cve_region"];?>"><?=$_SESSION["usuario"];?></option>
                </select>
                 <i></i><b>Numero de Escuelas Faltantes por Contestar Encuesta segun su Oficina Regional: <?php echo $escuelas_faltantes;?> </b>
  		    </div>
        <button type="button" class="btn btn-warning" id="btn_buscar">Buscar</button>
  		  </div>
      </form>
      <div id="resultado" class="Content" style="display: none;">  
                <table class="table table-bordered table-striped table-condensed flip-content" id="table_reporte">
            <?php if($_SESSION["cve_region"]=="123"){?>
                        <a href="_actions/exportar_encuestas.php" class="btn green-meadow">Exportar Archivo <i class="fa fa-file-excel-o"></i> </a>
            <?php } ?>
                    <thead class="flip-content">
                        <tr>
                            <td>CCT</td>
                            <td>Turno</td>
                            <td>Nombre</td>
                            <td>Municipio</td>
                            <td>Localidad</td>
                            <td>Nivel</td>
                            <td>Zona Escolar</td>
                            <td>Jefatura</td>
                            <td>Ver Encuesta</td>
                        </tr>
                    </thead>
                </table>
                <div>
            </div>
        </div>
    </div>
</div>

<!--        MODAL        -->
<div id="confirmar_delete" class="modal fade in" data-backdrop="static" data-keyboard="false" style="display: none; padding-right: 21px;">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmar</h4>
            </div>
            <div class="modal-body">
                <p>
                    ¿Seguro quiere eliminar este registro?
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn default">cancelar</button>
                <button type="button" class="btn green" onclick="fn_eliminar_reg()">eliminar</button>
            </div>
        </div>
    </div>

<div  class="modal-scrollable">
<div class="modal fade in" id="ModalVer" tabindex="-1" data-width="760" style="overflow: scroll; display: block; width: 760px; margin-left: -379px;" role="dialog" >
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Detalle de la Encuesta</h4>
                                        </div>
                                        <div class="modal-body">
                                             <form role="form" class="form-horizontal"  id="actualizar_encuesta" name="actualizar_encuesta" method="post" action="" autocomplete="on" enctype="multipart/form-data">
                                        <div class="form-body">
                                         <input type="hidden" class="form-control" id="folio" name="folio">
                                        <input type="hidden" class="form-control" id="region_dos" name="region_dos">
                                        <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">CCT</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="cct" name="cct">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa la Clave de Centro (CCT)</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Turno</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="turno" name="turno">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa el Turno de la Escuela</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Nombre</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="nombre" name="nombre">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa el Nombre de la Escuela</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Municipio</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="municipio" name="municipio">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa el Municipio</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Localidad</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="localidad" name="localidad">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa la Localidad</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Nivel</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="nivel" name="nivel">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa el Nivel Educativo</span>
                                                       
                                                    </div>
                                                </div>
                                            </div> <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Zona Escolar</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="zona_escolar" name="zona_escolar">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa la Zona Escolar</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group form-md-line-input has-danger">
                                                <label class="col-md-1 control-label" for="form_control_1">Jefatura</label>
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" id="jefatura" name="jefatura">
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block">Ingresa la Jefatura</span>
                                                       
                                                    </div>
                                                </div>
                                            </div>

										 <label style="text-align: center;"><b>PLANTEL VANDALIZADO</b></label><br><br>
										<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">¿El plantel ha sido vandalizado?</label>
                                                <div class="col-md-3">
                                                    <div class="md-checkbox-inline">
                                                        <div class="md-checkbox">
                                                            <input type="checkbox" id="checkbox1" name="checkbox1"class="md-check" value="SI">
                                                            <label for="checkbox1">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> SI </label>
                                                        </div>
                                                        <div class="md-checkbox has-error">
                                                            <input type="checkbox" id="checkbox2" name="checkbox1" class="md-check" value="NO">
                                                            <label for="checkbox2">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> NO </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">En caso de ser, SI. Especifique: </label>
                                                <div class="col-md-10">
                                                    <textarea class="form-control" rows="3" placeholder="Especifique" id="vandalizado_si" name="vandalizado_si"></textarea>
                                                    <div class="form-control-focus"> </div>
                                                </div>
                                            </div>
                                              <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">En caso de que la respuesta anterior sea afirmativa, Indique cuales daños han sido atendidos: </label>
                                                <div class="col-md-10">
                                                    <textarea class="form-control" rows="3" placeholder="Especifique" id="atendido" name="atendido"></textarea>
                                                    <div class="form-control-focus"> </div>
                                                </div>
                                            </div>
											 <label class="col-md-6.5 control-label" for="form_control_1"><b>SISTEMA ELÉCTRICO E ILUMINACIÓN</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Actualmente, ¿Cuenta con servicio de Energia Eléctrica?</label>
                                                <div class="col-md-3">
                                                    <div class="md-checkbox-inline">
                                                        <div class="md-checkbox">
                                                            <input type="checkbox" id="checkbox3"  name="checkbox2" class="md-check" value="SI">
                                                            <label for="checkbox3">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> SI </label>
                                                        </div>
                                                        <div class="md-checkbox has-error">
                                                            <input type="checkbox" id="checkbox4"  name="checkbox2" class="md-check" value="NO">
                                                            <label for="checkbox4">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> NO </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Focos o Lamparas</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio1" name="radio1" class="md-radiobtn" value="Bueno">
                                                            <label for="radio1">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio2" name="radio1" class="md-radiobtn" value="Regular">
                                                            <label for="radio2">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio3" name="radio1" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio3">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Contactos</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio4" name="radio2" class="md-radiobtn" value="Bueno">
                                                            <label for="radio4">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio5" name="radio2" class="md-radiobtn" value="Regular">
                                                            <label for="radio5">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio6" name="radio2" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio6">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Red Eléctrica Interior (Aulas)</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio7" name="radio3" class="md-radiobtn" value="Bueno">
                                                            <label for="radio7">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio8" name="radio3" class="md-radiobtn" value="Regular">
                                                            <label for="radio8">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio9" name="radio3" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio9">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Red Eléctrica Exterior (Registros)</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio10" name="radio4" class="md-radiobtn" value="Bueno">
                                                            <label for="radio10">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio11" name="radio4" class="md-radiobtn" value="Regular">
                                                            <label for="radio11">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio12" name="radio4" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio12">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											 <label class="col-md-6.5 control-label" for="form_control_1"><b>SISTEMA DE AGUA POTABLE</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Actualmente, ¿Cuenta con servicio de Agua Potable?</label>
                                                <div class="col-md-3">
                                                    <div class="md-checkbox-inline">
                                                        <div class="md-checkbox">
                                                            <input type="checkbox" id="checkbox5" name="checkbox3" class="md-check" value="SI">
                                                            <label for="checkbox5">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> SI </label>
                                                        </div>
                                                        <div class="md-checkbox has-error">
                                                            <input type="checkbox" id="checkbox6" name="checkbox3"class="md-check" value="NO">
                                                            <label for="checkbox6">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> NO </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Red de Agua Potable</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio13" name="radio5" class="md-radiobtn" value="Bueno">
                                                            <label for="radio13">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio14" name="radio5" class="md-radiobtn" value="Regular">
                                                            <label for="radio14">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio15" name="radio5" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio15">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<label class="col-md-6.5 control-label" for="form_control_1"><b>SISTEMA DE DRENAJE</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Actualmente, ¿Cuenta con servicio de Drenaje?</label>
                                                <div class="col-md-3">
                                                    <div class="md-checkbox-inline">
                                                        <div class="md-checkbox">
                                                            <input type="checkbox" id="checkbox7" name="checkbox4" class="md-check" value="SI">
                                                            <label for="checkbox7">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> SI </label>
                                                        </div>
                                                        <div class="md-checkbox has-error">
                                                            <input type="checkbox" id="checkbox8"  name="checkbox4"  class="md-check" value="NO">
                                                            <label for="checkbox8">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> NO </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Red de Drenaje Externa (Registros)</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio16" name="radio6" class="md-radiobtn" value="Bueno">
                                                            <label for="radio16">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio17" name="radio6" class="md-radiobtn" value="Regular">
                                                            <label for="radio17">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio18" name="radio6" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio18">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Fosa Séptica</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio19" name="radio7" class="md-radiobtn" value="Bueno">
                                                            <label for="radio19">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio20" name="radio7" class="md-radiobtn" value="Regular">
                                                            <label for="radio20">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio21" name="radio7" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio21">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<label class="col-md-6.5 control-label" for="form_control_1"><b>SERVICIOS DE SANITARIOS</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Inodoro</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio22" name="radio8" class="md-radiobtn" value="Bueno">
                                                            <label for="radio22">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio23" name="radio8" class="md-radiobtn" value="Regular">
                                                            <label for="radio23">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio24" name="radio8" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio24">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Mingitorios</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio25" name="radio9" class="md-radiobtn" value="Bueno">
                                                            <label for="radio25">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio26" name="radio9" class="md-radiobtn" value="Regular">
                                                            <label for="radio26">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio27" name="radio9" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio27">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Lavamanos</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio28" name="radio10" class="md-radiobtn" value="Bueno">
                                                            <label for="radio28">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio29" name="radio10" class="md-radiobtn" value="Regular">
                                                            <label for="radio29">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio30" name="radio10" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio30">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<label class="col-md-6.5 control-label" for="form_control_1"><b>PUERTAS Y CANCELERÍA</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Puertas</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio31" name="radio11" class="md-radiobtn" value="Bueno">
                                                            <label for="radio31">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio32" name="radio11" class="md-radiobtn" value="Regular">
                                                            <label for="radio32">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio33" name="radio11" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio33">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Chapas</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio34" name="radio12" class="md-radiobtn" value="Bueno">
                                                            <label for="radio34">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio35" name="radio12" class="md-radiobtn" value="Regular">
                                                            <label for="radio35">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio36" name="radio12" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio36">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Vidrios</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio37" name="radio13" class="md-radiobtn" value="Bueno">
                                                            <label for="radio37">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio38" name="radio13" class="md-radiobtn" value="Regular">
                                                            <label for="radio38">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio39" name="radio13" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio39">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Protecciones</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio40" name="radio14" class="md-radiobtn" value="Bueno">
                                                            <label for="radio40">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Bueno</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio41" name="radio14" class="md-radiobtn" value="Regular">
                                                            <label for="radio41">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Regular</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio42" name="radio14" class="md-radiobtn" value="SinFuncionar">
                                                            <label for="radio42">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Sin Funcionar</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<label class="col-md-6.5 control-label" for="form_control_1"><b>HIGIENE Y LIMPIEZA</b></label><br><br>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Maleza</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio43" name="radio15" class="md-radiobtn" value="Mucha">
                                                            <label for="radio43">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Mucha</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio44" name="radio15" class="md-radiobtn" value="Poca">
                                                            <label for="radio44">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Poca</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio45" name="radio15" class="md-radiobtn" value="Nada">
                                                            <label for="radio45">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Nada</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Basura</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio46" name="radio16" class="md-radiobtn" value="Mucha">
                                                            <label for="radio46">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Mucha</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio47" name="radio16" class="md-radiobtn" value="Poca">
                                                            <label for="radio47">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Poca</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio48" name="radio16" class="md-radiobtn" value="Nada">
                                                            <label for="radio48">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Nada</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group form-md-line-input">
                                                <label class="col-md-2 control-label" for="form_control_1">Escombro</label>
                                                <div class="col-md-10">
                                                    <div class="md-radio-inline">
                                                        <div class="md-radio">
                                                            <input type="radio" id="radio49" name="radio17" class="md-radiobtn" value="Mucha">
                                                            <label for="radio49">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Mucha</label>
                                                        </div>
                                                        <div class="md-radio has-error">
                                                            <input type="radio" id="radio50" name="radio17" class="md-radiobtn" value="Poca">
                                                            <label for="radio50">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Poca</label>
                                                        </div>
                                                        <div class="md-radio has-warning">
                                                            <input type="radio" id="radio51" name="radio17" class="md-radiobtn" value="Nada">
                                                            <label for="radio51">
                                                                <span class="inc"></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span>Nada</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-outline dark" id="btnCerrarEncuesta" name="btnCerrarEncuesta">Close</button>
                                            <button type="submit" class="btn btn-outline dark" id="BtnActualizarEncuesta" name="BtnActualizarEncuesta">Actualizar</button>
                                        </div>
                        </div>
</div>
<script>

    /*************                  
        variables globales
    ************** */

$("#btn_buscar").click(function(){
  var datos = {
          region: $("#region_enviar").val()
        };
usuario = "<?=$_SESSION['cve_region']?>";
console.log(usuario);
if(usuario==='123'){
var table = $("#table_reporte").DataTable({
    order: [
        [0, "desc"]
    ],
    "responsive": true,
    "scrollX": true,
    "scrollY": true,
    "destroy": true,
    lengthMenu: [
        [10, 20, 30, -1],
        [10, 20, 30, 'All']
    ],
    "ajax": {
        "url": "_actions/get_reporte.php",
    },
    "columns": [
        {
            "data": "cct"
        },
        {
            "data": "turno"
        },
        {
            "data": "nombre"
        },
        {
            "data": "municipio"
        },
        {
            "data": "localidad"
        },
        {
            "data": "nivel"
        },
        {
            "data": "zona_escolar"
        },
        {
            "data": "jefatura"
        },
       {
            "sortable": false,
            "render": function(data, type, full, meta) {
                return `
                    <button type="button" class="btn yellow" onclick="fn_ver('${full.folio}','${full.cct}','${full.turno}','${full.nombre}','${full.municipio}','${full.localidad}','${full.region}','${full.nivel}'
                    ,'${full.zona_escolar}','${full.jefatura}','${full.vandalizado}','${full.vandalizado_si}','${full.atendido}','${full.servicio_electrico}','${full.focos}','${full.contactos}','${full.red_electrica_interior}','${full.red_electrica_exterior}','${full.servicio_agua}','${full.red_agua}','${full.servicio_drenaje}','${full.red_drenaje}'
                    ,'${full.fosa_septica}','${full.inodoro}','${full.mingitorios}','${full.lavamanos}','${full.puertas}','${full.chapas}','${full.vidrios}','${full.protecciones}','${full.maleza}','${full.basura}','${full.escombro}')"><i class="fa fa-edit"></i> Ver </button>                    
                `;
            }
        },
        
    ]
});
$("#resultado").css("display", "block");
}else{
var table = $("#table_reporte").DataTable({
    order: [
        [0, "desc"]
    ],
    "responsive": true,
    "scrollX": true,
    "scrollY": true,
    "destroy": true,
    lengthMenu: [
        [10, 20, 30, -1],
        [10, 20, 30, 'All']
    ],
    "ajax": {
        "url": "_actions/get_reporte_by_region.php",
        "type": "post",
         "data":datos
    },
    "columns": [
        {
            "data": "cct"
        },
        {
            "data": "turno"
        },
        {
            "data": "nombre"
        },
        {
            "data": "municipio"
        },
        {
            "data": "localidad"
        },
        {
            "data": "nivel"
        },
        {
            "data": "zona_escolar"
        },
        {
            "data": "jefatura"
        },
       {
            "sortable": false,
            "render": function(data, type, full, meta) {
                return `
                    <button type="button" class="btn yellow" onclick="fn_ver('${full.folio}','${full.cct}','${full.turno}','${full.nombre}','${full.municipio}','${full.localidad}','${full.region}','${full.nivel}'
                    ,'${full.zona_escolar}','${full.jefatura}','${full.vandalizado}','${full.vandalizado_si}','${full.atendido}','${full.servicio_electrico}','${full.focos}','${full.contactos}','${full.red_electrica_interior}','${full.red_electrica_exterior}','${full.servicio_agua}','${full.red_agua}','${full.servicio_drenaje}','${full.red_drenaje}'
                    ,'${full.fosa_septica}','${full.inodoro}','${full.mingitorios}','${full.lavamanos}','${full.puertas}','${full.chapas}','${full.vidrios}','${full.protecciones}','${full.maleza}','${full.basura}','${full.escombro}')"><i class="fa fa-edit"></i> Ver </button>                    
                `;
            }
        },
        
    ]
});
}
$("#resultado").css("display", "block");
});



function fn_ver(folio,cct, turno, nombre, municipio, localidad, region, nivel, zona_escolar, jefatura, vandalizado, vandalizado_si, atendido,
                servicio_electrico, focos, contactos, red_electrica_interior, red_electrica_exterior, servicio_agua, red_agua, servicio_drenaje, red_drenaje, fosa_septica, 
                inodoro, mingitorios, lavamanos, puertas, chapas, vidrios, protecciones, maleza, basura, escombro) {
                $('#ModalVer').modal("show");
                $('#folio').val(folio);
                $('#cct').val(cct);
                $('#turno').val(turno);
                $('#nombre').val(nombre);
                $('#municipio').val(municipio);
                $('#localidad').val(localidad);
                 $('#region_dos').val(region);
                $('#nivel').val(nivel);
                $('#zona_escolar').val(zona_escolar);
                $('#jefatura').val(jefatura);
                $('#vandalizado_si').val(vandalizado_si);
                $('#atendido').val(atendido);
                $("input[name=radio1][value="+focos+"]").prop('checked', true);
                $("input[name=radio2][value="+contactos+"]").prop('checked', true);
                $("input[name=radio3][value="+red_electrica_interior+"]").prop('checked', true);
                $("input[name=radio4][value="+red_electrica_exterior+"]").prop('checked', true);
                $("input[name=radio5][value="+red_agua+"]").prop('checked', true);
                $("input[name=radio6][value="+red_drenaje+"]").prop('checked', true);
                $("input[name=radio7][value="+fosa_septica+"]").prop('checked', true);
                $("input[name=radio8][value="+inodoro+"]").prop('checked', true);
                $("input[name=radio9][value="+mingitorios+"]").prop('checked', true);
                $("input[name=radio10][value="+lavamanos+"]").prop('checked', true);
                $("input[name=radio11][value="+puertas+"]").prop('checked', true);
                $("input[name=radio12][value="+chapas+"]").prop('checked', true);
                $("input[name=radio13][value="+vidrios+"]").prop('checked', true);
                $("input[name=radio14][value="+protecciones+"]").prop('checked', true);
                $("input[name=radio15][value="+maleza+"]").prop('checked', true);
                $("input[name=radio16][value="+basura+"]").prop('checked', true);
                $("input[name=radio17][value="+escombro+"]").prop('checked', true); 
                $("input[name=checkbox1][value="+vandalizado+"]").prop('checked', true);   
                $("input[name=checkbox2][value="+servicio_electrico+"]").prop('checked', true); 
                $("input[name=checkbox3][value="+servicio_agua+"]").prop('checked', true); 
                $("input[name=checkbox4][value="+servicio_drenaje+"]").prop('checked', true); 
            };




             $('#BtnActualizarEncuesta').click(function (e) {  
                    var formData = new FormData(document.getElementById("actualizar_encuesta"));
                       $.ajax({
                           url: '_actions/update_encuesta.php',
                           type: 'POST',
                           data: formData,
                           processData: false,  // tell jQuery not to process the data
                           contentType: false, // tell jQuery not to set contentType
                           success: function (resultado) {
                               console.log(resultado);
                                   alert(resultado);
                                   $('#btnCerrarEncuesta').click();
								   $("#table_reporte").DataTable().ajax.reload();
                               
                           },
                           error: function (xhr, desc, err) {
                               console.log(xhr);
                               console.log("Details:" + desc + "\nError" + err);
                           }
                       });
                   
           });
   //TERMINA LA FUNCION PARA ACTUALIZAR LA MODALIDAD DEL ALUMNO 

    



</script>