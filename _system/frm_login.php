
<!------ Include the above in your HEAD tag ---------->
<meta charset="UTF-8">
<div class="login-reg-panel">
    <!-- <div class="login-info-box">
        <h2>Have an account?</h2>
        <p>Lorem ipsum dolor sit amet</p>
        <label id="label-register" for="log-reg-show">Login</label>
        <input type="radio" name="active-log-panel" id="log-reg-show" checked="checked">
    </div> -->

    <div class="register-info-box">
        
        <!-- <h2>Don't have an account?</h2>
        <p>Lorem ipsum dolor sit amet</p>
        <label id="label-login" for="log-login-show">Register</label>
        <input type="radio" name="active-log-panel" id="log-login-show"> -->
    </div>

    <div class="white-panel">
        <div class="login-show">
            <h3 class="form-title">Accede para ver tus Encuestas</h3>
            <form id="form_login" action="javascript:;">
            <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">REGION</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                         <select id="usr" name="usr" class="form-control placeholder-no-fix" >
                         <option>Seleccione su Regi&oacute;n</option>
                         <option value="ADMINISTRADOR">Administrador</option>
                          <option value="OF_ACUNA">Servicios Regionales Acuña</option>
                           <option value="OF_PIEDRAS">Servicios Regionales Piedras Negras</option>
                            <option value="OF_ALLENDE">Servicios Regionales Allende</option>
                             <option value="OF_NROSITA">Servicios Regionales Nueva Rosita</option>
                              <option value="OF_SABINAS">Servicios Regionales Sabinas</option>
                               <option value="OF_CUATRO">Servicios Regionales Cuatrocienegas</option>
                                <option value="OF_MONCLOVA">Servicios Regionales Monclova</option>
                                 <option value="OF_MATAMOROS">Servicios Regionales Matamoros</option>
                                  <option value="OF_SANPEDRO">Servicios Regionales San Pedro</option>
                                   <option value="OF_FCOIMADERO">Servicios Educativos Fco. I Madero</option>
                                    <option value="OF_PARRAS">Servicios Regionales Parras de la Fuente</option>
                                     <option value="OF_SALTILLO">Servicios Regionales Saltillo</option>
                                      <option value="OF_TORREON">Servicios Educativos Laguna</option>
                         </select>
                         </div>
            </div>
            <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">CONTRASE&nacute;A</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input id="psw" name="psw"class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="CONTRASEÑA" name="password"> </div>
            </div>
                <input type="submit" class="btn btn-primary btn-block" style="background-color: orangered;" value="Acceder">
            </form>
        </div>
        <!-- <div class="register-show">
            <h2>REGISTER</h2>
            <input type="text" placeholder="Email">
            <input type="password" placeholder="Password">
            <input type="password" placeholder="Confirm Password">
            <input type="button" value="Register">
        </div> -->
    </div>
</div>

<script>

    $(document).ready(function () {
        $('.login-info-box').fadeOut();
        $('.login-show').addClass('show-log-panel');
    });


    // $('.login-reg-panel input[type="radio"]').on('change', function () {
    //     if ($('#log-login-show').is(':checked')) {
    //         $('.register-info-box').fadeOut();
    //         $('.login-info-box').fadeIn();

    //         $('.white-panel').addClass('right-log');
    //         $('.register-show').addClass('show-log-panel');
    //         $('.login-show').removeClass('show-log-panel');

    //     }
    //     else if ($('#log-reg-show').is(':checked')) {
    //         $('.register-info-box').fadeIn();
    //         $('.login-info-box').fadeOut();

    //         $('.white-panel').removeClass('right-log');

    //         $('.login-show').addClass('show-log-panel');
    //         $('.register-show').removeClass('show-log-panel');
    //     }
    // });

    // JAVA SCRIPT TO VERIFY USER AND PASSWORD
    $("#form_login").submit(function (e) {
        var usr = $("#usr").val();
        var psw = $("#psw").val();
        // console.log("usr:"+usr);
        // console.log("psw:"+psw);
        if(usr.trim() == ""){
            alert('¡Ingresar usuario!');
            return false;
        }
        if(psw.trim() == ""){
            alert('¡Ingresar contraseña!');
            return false;
        }

        var data = new FormData(form_login);

        $.ajax({
            url: '_actions/validar_usuario.php',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,            
            data: data            
        })
        .done(function (response) {
            console.log(response);
            if(response.procede == 1 && response.status == 1){
               	$("#principal").load('_system/frm_reporte.php',function(){			
		});
            }
            else if(response.procede == 0 && response.status == 0){
                alert('usuario no encontrado');
            }

        })
        .fail(function (xhr, textStatus, err) {
            console.log("Details: " + textStatus + "\nError:" + err);
            if (xhr.status === 0) {
                alert('No conectado: Verificar internet.');
            } else if (xhr.status == 404) {
                alert('Pagina no encontrada [404]');
            } else if (xhr.status == 500) {
                alert('Error en el servidor [500].');
            } else if (textStatus === 'parsererror') {
                alert('El JSON solicitado ha fallado');
            } else if (textStatus === 'timeout') {
                alert('Tiempo de respuesta exedido.');
            } else if (textStatus === 'abort') {
                alert('Solicitud de ajax abortada.');
            } else {
                alert('Error desconocido: ' + xhr.responseText);
            }
        })
        .always(function () {
            console.log("complete");
        });

    });


</script>