$("#escuelas").click( function(){
    $("#principal-content").load("_system/frm_escuela.php");
});
$("#instituto_de_becas").click( function(){
    $("#principal-content").load("_system/frm_instituto_de_becas.php");
});
$("#documentos").click( function(){
	$("#principal-content").load("_system/frm_documentos.php");
});
$("#curp").click( function(){
	$("#principal-content").load("_system/frm_buscar_alumno_curp.php");
});
$("#nombre").click( function(){
  $("#principal-content").load("_system/frm_buscar_alumno_nombre.php");
});
$("#folio").click( function(){
	$("#principal-content").load("_system/frm_buscar_alumno_folio.php");
});
$("#entre_porcentajes").click( function(){
  $("#principal-content").load("_system/frm_buscar_alumno_entre_porcentajes.php");
});
$("#informacion").click( function(){
	$("#principal-content").load("_system/frm_informacion.php");
});
$("#cuotas").click( function(){
	$("#principal-content").load("_system/frm_cuotas.php");
});
$("#becas").click( function(){
	$("#principal-content").load("_system/frm_becas.php");
});
$("#alumnos").click( function(){
	$("#principal-content").load("_system/frm_alumnos.php");
});
$("#agregar_alumno").click( function(){
	$("#principal-content").load("_system/frm_agregar_alumno.php");
});
$("#comite").click( function(){
	$("#principal-content").load("_system/frm_comite.php");
});

$("#programas").click( function(){
  $("#principal-content").load("_system/frm_programas.php");
});
$("#usuarios_permisos").click( function(){
  $("#principal-content").load("_system/frm_usuarios.php");
});

$("#oficio_beca").click( function(){
  $("#principal-content").load("_system/frm_oficio_beca.php");
});

$("#menu_registro").click( function(){
  $("#principal-content").load("_system/frm_registro.php");
});

/*----------------------------------------C E R R A R     S E S I O N------------------------------------------*/
$("#btn_cerrar_sesion").click(function() {
	$.ajax({
		url: '_actions/cerrar_sesion.php',
		type: 'POST',
		dataType: 'html',
		data: {sesion: 'close'}
	  })	
	.done(function(response) {			
		$("#desconectarse").fadeOut('500');
		$(".modal-backdrop").remove();
		console.log("success");
		window.location.href = "../seguridad/";
	})
	.fail(function(xhr, desc, err) {
              console.log(xhr);
              console.log("Details: " + desc + "\nError:" + err);
	})
	.always(function() {
		console.log("complete");
	});		
});
/*----------------------------------------------------------------------------------------------------------*/
 //SELECT CURP
 $('#select_curp').submit(function (e){  
    //alert('si ');
    var data = new FormData(this); 
    console.log(data);
    $.ajax({
      url: '_actions/alumnos_por_escuela.php',
      data: data,
      processData: false, 
      contentType: false,
      type: 'POST',
      success: function (resultado) {
      // alert('resultados');
      $('#resultado_curp').html(resultado);
  }
});
    e.preventDefault(); 
});

$('#select_folio').submit(function (e){  
    //alert('si ');
    var data = new FormData(this); 
    console.log(data);
    $.ajax({
      url: '_actions/alumnos_por_folio.php',
      data: data,
      processData: false, 
      contentType: false,
      type: 'POST',
      success: function (resultado) {
      // alert('resultados');
      $('#resultado_folio').html(resultado);
  }
});
    e.preventDefault(); 
});

/*$('#select_grado_grupo').submit(function (e){  
    //alert('si ');
    var data = new FormData(this); 
    console.log(data);
    $.ajax({
      url: '_actions/lista_alumnos_grado_seccion.php',
      data: data,
      processData: false, 
      contentType: false,
      type: 'POST',
      success: function (resultado) {
      // alert('resultados');
      $('#resultado_grado_grupo').html(resultado);
  }
});
    e.preventDefault(); 
});
/*----------------------------------------------------------------------------------------------------------*/
 //SELECT NOMBRE
$('#select_nombre').submit(function (e){  
    //alert('si ');
    var data = new FormData(this); 
    console.log(data);
    $.ajax({
      url: '_actions/alumnos_por_nombre.php',
      data: data,
      processData: false, 
      contentType: false,
      type: 'POST',
      success: function (resultado) {
      // alert('resultados');
      $('#resultado_nombre').html(resultado);
  }
});
    e.preventDefault(); 
});
$('#select_paterno').submit(function (e){  
    //alert('si ');
    var data = new FormData(this); 
    console.log(data);
    $.ajax({
      url: '_actions/alumnos_por_nombre.php',
      data: data,
      processData: false, 
      contentType: false,
      type: 'POST',
      success: function (resultado) {
      // alert('resultados');
      $('#resultado_paterno').html(resultado);
  }
});
    e.preventDefault(); 
});
$('#select_materno').submit(function (e){  
    //alert('si ');
    var data = new FormData(this); 
    console.log(data);
    $.ajax({
      url: '_actions/alumnos_por_nombre.php',
      data: data,
      processData: false, 
      contentType: false,
      type: 'POST',
      success: function (resultado) {
      // alert('resultados');
      $('#resultado_materno').html(resultado);
  }
});
    e.preventDefault(); 
});

//*******************//
$('#form_busqueda').submit(function (e){ 
    var data = new FormData(this);
    $.ajax({
      url: '_actions/alumnos_por_porcentajes.php',
      data: data,
      processData: false, 
      contentType: false,
      type: 'POST',
      success: function (response) {
      $('#resultado').html(response);
  }
});
    e.preventDefault(); 
});

function save_percent_cct(val,cct,turno,curp){
    $("#mensajito").html("&nbsp;");
      if (val.value<20){
          $("#mensajito").html("El PORCENTAJE MINIMO ADMITIDO ES 20");
          $(val).css('background-color', '#F8E0E0');
          $(val).focus();
      } else {
      $.ajax({
      url: '_actions/actualiza_porcentaje_cct.php',
      type: 'POST',
      dataType: 'html',
      data: {porcentaje_cambio:val.value, cct:cct, turno:turno, curp:curp},
      success: function (response) {
              $(val).css('background-color', 'white');
          if (response==1){
              $("#mensajito").html("EL PORCENTAJE HA SIDO ACTUALIZADO");
          } else {
              $("#mensajito").html("NO REALIZÓ CAMBIOS EN PORCENTAJE");
          }
      }
    });
  }
  }


//**********************  VALIDACION  ************************************************////
  function soloNumeros(e) {
      var key = window.Event ? e.which : e.keyCode
      return ((key >= 48 && key <= 57) || (key==8));
  }

  function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
  