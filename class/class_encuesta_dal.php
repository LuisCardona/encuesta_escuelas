 <?php 

include ('class_conexion.php');//Clase de conexion 
include ('class_encuesta.php');
 
 
class encuesta_dal extends class_db_beca 
{ 
 
	function __construct() 
	{ 
		parent::__construct(); 
	} 
 
	function __destruct() 
	{ 
		parent::__destruct(); 
    } 



     public function get_all(){
        $this->set_sql("SELECT * FROM encuesta "); 

        $rs = mysqli_query($this->db_conn, $this->db_query) 
        or $this->throw_ex(mysqli_error($this->db_conn)); 
        
        $total_de_registro = mysqli_num_rows($rs); 
        $i = 0; 
        $lista = null; 
        if ($total_de_registro > 0) { 
            while ($renglon = mysqli_fetch_assoc($rs)) { 
                $obj = new encuesta();
                $obj-> setfolio($renglon['folio']); 
                $obj-> setcct($renglon['cct']); 
                $obj-> setturno($renglon['turno']); 
                $obj-> setnombre($renglon['nombre']); 
                $obj-> setmunicipio($renglon['municipio']); 
                $obj-> setlocalidad($renglon['localidad']); 
                 $obj-> setregion($renglon['region']); 
                $obj-> setnivel($renglon['nivel']); 
                $obj-> setzona_escolar($renglon['zona_escolar']); 
                $obj-> setjefatura($renglon['jefatura']);
                $obj-> setvandalizado($renglon['vandalizado']);
                $obj-> setvandalizado_si($renglon['vandalizado_si']);
                   $obj-> setatendido($renglon['atendido']);
                $obj-> setservicio_electrico($renglon['servicio_electrico']);
                $obj-> setfocos($renglon['focos']);
                $obj-> setcontactos($renglon['contactos']);
                $obj-> setred_electrica_interior($renglon['red_electrica_interior']);
                $obj-> setred_electrica_exterior($renglon['red_electrica_exterior']);
                
              
                $obj-> setservicio_agua($renglon['servicio_agua']); 
                $obj-> setred_agua($renglon['red_agua']); 
                $obj-> setservicio_drenaje($renglon['servicio_drenaje']); 
                $obj-> setred_drenaje($renglon['red_drenaje']);
                $obj-> setfosa_septica($renglon['fosa_septica']);
                $obj-> setinodoro($renglon['inodoro']);
                $obj-> setmingitorios($renglon['mingitorios']);
                $obj-> setlavamanos($renglon['lavamanos']); 
                $obj-> setpuertas($renglon['puertas']);

                $obj-> setchapas($renglon['chapas']);
                $obj-> setvidrios($renglon['vidrios']);
                $obj-> setprotecciones($renglon['protecciones']);
                $obj-> setmaleza($renglon['maleza']);
                $obj-> setbasura($renglon['basura']);
                $obj-> setescombro($renglon['escombro']);
               

                $i++; 
                $lista[$i] = $obj; 
                unset($obj); 
            } 
        } 
        return $lista; 
    } 

    public function get_by_id($folio){
        $this->set_sql("SELECT * FROM encuesta
                        WHERE folio='$folio' "); 

        $rs = mysqli_query($this->db_conn, $this->db_query) 
        or $this->throw_ex(mysqli_error($this->db_conn)); 
        
        $total_de_registro = mysqli_num_rows($rs); 
        $i = 0; 
        $lista = null; 
        if ($total_de_registro > 0) { 
            while ($renglon = mysqli_fetch_assoc($rs)) { 
                $obj = new encuesta();
                $obj-> setfolio($renglon['folio']); 
                $obj-> setcct($renglon['cct']); 
                $obj-> setturno($renglon['turno']); 
                $obj-> setnombre($renglon['nombre']); 
                $obj-> setmunicipio($renglon['municipio']); 
                $obj-> setlocalidad($renglon['localidad']); 
                $obj-> setnivel($renglon['nivel']); 
                $obj-> setzona_escolar($renglon['zona_escolar']); 
                $obj-> setjefatura($renglon['jefatura']);
                $obj-> setvandalizado($renglon['vandalizado']);
                $obj-> setvandalizado_si($renglon['vandalizado_si']);
                 $obj-> setatendido($renglon['atendido']);
                $obj-> setservicio_electrico($renglon['servicio_electrico']);
                $obj-> setfocos($renglon['focos']);
                $obj-> setcontactos($renglon['contactos']);
                $obj-> setred_electrica_interior($renglon['red_electrica_interior']);
                $obj-> setred_electrica_exterior($renglon['red_electrica_exterior']);
                
              
                $obj-> setservicio_agua($renglon['servicio_agua']); 
                $obj-> setred_agua($renglon['red_agua']); 
                $obj-> setservicio_drenaje($renglon['servicio_drenaje']); 
                $obj-> setred_drenaje($renglon['red_drenaje']);
                $obj-> setfosa_septica($renglon['fosa_septica']);
                $obj-> setinodoro($renglon['inodoro']);
                $obj-> setmingitorios($renglon['mingitorios']);
                $obj-> setlavamanos($renglon['lavamanos']); 
                $obj-> setpuertas($renglon['puertas']);

                $obj-> setchapas($renglon['chapas']);
                $obj-> setvidrios($renglon['vidrios']);
                $obj-> setprotecciones($renglon['protecciones']);
                $obj-> setmaleza($renglon['maleza']);
                $obj-> setbasura($renglon['basura']);
                $obj-> setescombro($renglon['escombro']);
               

                $i++; 
                $lista[$i] = $obj; 
                unset($obj); 
            } 
        } 
        return $lista; 
    }

     public function get_by_region($region){
        $this->set_sql("SELECT * 
                        FROM encuesta 
                        WHERE region='$region'"); 

        $rs = mysqli_query($this->db_conn, $this->db_query) 
        or $this->throw_ex(mysqli_error($this->db_conn)); 
        
        $total_de_registro = mysqli_num_rows($rs); 
        $i = 0; 
        $lista = null; 
        if ($total_de_registro > 0) { 
            while ($renglon = mysqli_fetch_assoc($rs)) { 
                $obj = new encuesta();
                $obj-> setfolio($renglon['folio']); 
                $obj-> setcct($renglon['cct']); 
                $obj-> setturno($renglon['turno']); 
                $obj-> setnombre($renglon['nombre']); 
                $obj-> setmunicipio($renglon['municipio']); 
                $obj-> setlocalidad($renglon['localidad']); 
                $obj-> setnivel($renglon['nivel']); 
                $obj-> setzona_escolar($renglon['zona_escolar']); 
                $obj-> setjefatura($renglon['jefatura']);
                $obj-> setvandalizado($renglon['vandalizado']);
                $obj-> setvandalizado_si($renglon['vandalizado_si']);
                $obj-> setatendido($renglon['atendido']);
                $obj-> setservicio_electrico($renglon['servicio_electrico']);
                $obj-> setfocos($renglon['focos']);
                $obj-> setcontactos($renglon['contactos']);
                $obj-> setred_electrica_interior($renglon['red_electrica_interior']);
                $obj-> setred_electrica_exterior($renglon['red_electrica_exterior']);
                
              
                $obj-> setservicio_agua($renglon['servicio_agua']); 
                $obj-> setred_agua($renglon['red_agua']); 
                $obj-> setservicio_drenaje($renglon['servicio_drenaje']); 
                $obj-> setred_drenaje($renglon['red_drenaje']);
                $obj-> setfosa_septica($renglon['fosa_septica']);
                $obj-> setinodoro($renglon['inodoro']);
                $obj-> setmingitorios($renglon['mingitorios']);
                $obj-> setlavamanos($renglon['lavamanos']); 
                $obj-> setpuertas($renglon['puertas']);

                $obj-> setchapas($renglon['chapas']);
                $obj-> setvidrios($renglon['vidrios']);
                $obj-> setprotecciones($renglon['protecciones']);
                $obj-> setmaleza($renglon['maleza']);
                $obj-> setbasura($renglon['basura']);
                $obj-> setescombro($renglon['escombro']);
               

                $i++; 
                $lista[$i] = $obj; 
                unset($obj); 
            } 
        } 
        return $lista; 
    }



     public function get_by_region_original($region){
        $this->set_sql("SELECT * 
                        FROM encuesta e
                        LEFT JOIN centros.vista_cct v on e.cct=v.cct 
                        WHERE v.subregion='$region'"); 

        $rs = mysqli_query($this->db_conn, $this->db_query) 
        or $this->throw_ex(mysqli_error($this->db_conn)); 
        
        $total_de_registro = mysqli_num_rows($rs); 
        $i = 0; 
        $lista = null; 
        if ($total_de_registro > 0) { 
            while ($renglon = mysqli_fetch_assoc($rs)) { 
                $obj = new encuesta();
                $obj-> setfolio($renglon['folio']); 
                $obj-> setcct($renglon['cct']); 
                $obj-> setturno($renglon['desc_turno']); 
                $obj-> setnombre($renglon['nombre']); 
                $obj-> setmunicipio($renglon['nombre_de_municipio']); 
                $obj-> setlocalidad($renglon['nombre_de_localidad']); 
                $obj-> setnivel($renglon['nivel']); 
                $obj-> setzona_escolar($renglon['zona_escolar']); 
                $obj-> setjefatura($renglon['jefatura']);
                $obj-> setvandalizado($renglon['vandalizado']);
                $obj-> setvandalizado_si($renglon['vandalizado_si']);
                $obj-> setatendido($renglon['atendido']);
                $obj-> setservicio_electrico($renglon['servicio_electrico']);
                $obj-> setfocos($renglon['focos']);
                $obj-> setcontactos($renglon['contactos']);
                $obj-> setred_electrica_interior($renglon['red_electrica_interior']);
                $obj-> setred_electrica_exterior($renglon['red_electrica_exterior']);
                
              
                $obj-> setservicio_agua($renglon['servicio_agua']); 
                $obj-> setred_agua($renglon['red_agua']); 
                $obj-> setservicio_drenaje($renglon['servicio_drenaje']); 
                $obj-> setred_drenaje($renglon['red_drenaje']);
                $obj-> setfosa_septica($renglon['fosa_septica']);
                $obj-> setinodoro($renglon['inodoro']);
                $obj-> setmingitorios($renglon['mingitorios']);
                $obj-> setlavamanos($renglon['lavamanos']); 
                $obj-> setpuertas($renglon['puertas']);

                $obj-> setchapas($renglon['chapas']);
                $obj-> setvidrios($renglon['vidrios']);
                $obj-> setprotecciones($renglon['protecciones']);
                $obj-> setmaleza($renglon['maleza']);
                $obj-> setbasura($renglon['basura']);
                $obj-> setescombro($renglon['escombro']);
               

                $i++; 
                $lista[$i] = $obj; 
                unset($obj); 
            } 
        } 
        return $lista; 
    }


    function get_datos_cct()
    {
            $this->set_sql(
                "SELECT nombre, turno, desc_turno, cct, domicilio, nombre_de_municipio
                FROM centros.vista_cct
                WHERE `status` in ('1','4') AND categoria_poblacion='1' AND turno LIKE '1%'
                AND SUBSTR(cct,3,3) in ('DJN','EJN','DPR','EPR','DES','EES','DST','EST','DSN','DTV')
                ");

            $rs = mysqli_query($this->db_conn,$this->db_query)
            or $this->throw_ex(mysqli_error($this->db_conn));
            
            $i = 0; 
            $lista = null; 
                while ($renglon = mysqli_fetch_assoc($rs)) { 
                    $lista[$i]['nombre'] = utf8_encode($renglon['nombre']); 
                    $lista[$i]['turno'] = utf8_encode($renglon['turno']);
                    $lista[$i]['desc_turno'] = utf8_encode($renglon['desc_turno']); 
                    $lista[$i]['cct'] = utf8_encode($renglon['cct']);
                    $lista[$i]['domicilio'] = utf8_encode($renglon['domicilio']);
                    $lista[$i]['nombre_de_municipio'] = utf8_encode($renglon['nombre_de_municipio']);
                    
                    
                    $i++; 
                } 
            
            return $lista; //Retorna un arreglo
    }



    function existe_cct($cct)
    {
        $this->set_sql(
            "SELECT count(*) as CUANTOS
            from encuesta
            where cct='$cct'"
        );

        $rs = mysqli_query($this->db_conn,$this->db_query)
            or $this->throw_ex(mysqli_error($this->db_conn));

        $total_de_registro = mysqli_num_rows($rs);
        $renglon = mysqli_fetch_assoc($rs);
        $cuantos =$renglon["CUANTOS"];
        return $cuantos;
    }

    
            function get_datos_cct2($cct)
            {
                    $this->set_sql(
                        "SELECT nombre, turno, desc_turno, cct, domicilio, nombre_de_municipio, zona_escolar, jefatura_de_sector, region
                        FROM centros.vista_cct
                        WHERE cct='$cct'
                        AND `status` in ('1','4')
                        AND identificador in ('ML','JN','PR','ES','ST','TV')
                        ");
        
                    $rs = mysqli_query($this->db_conn,$this->db_query)
                    or $this->throw_ex(mysqli_error($this->db_conn));
                    
                    $total_de_registro = mysqli_num_rows($rs); 
                    $renglon = mysqli_fetch_assoc($rs);
                            $datos["nombre"] = utf8_encode($renglon["nombre"]); 
                            $datos["turno"] = utf8_encode($renglon["turno"]);
                            $datos["desc_turno"] = utf8_encode($renglon["desc_turno"]); 
                            $datos["cct"] = utf8_encode($renglon["cct"]);
                            $datos["domicilio"] = utf8_encode($renglon["domicilio"]);
                            $datos["nombre_de_municipio"] = utf8_encode($renglon["nombre_de_municipio"]);
                             $datos["zona_escolar"] = utf8_encode($renglon["zona_escolar"]);
                             $datos["jefatura_de_sector"] = utf8_encode($renglon["jefatura_de_sector"]);
                              $datos["region"] = utf8_encode($renglon["region"]); 
                             $cct=$renglon["cct"];
                             $identificador=substr($cct,3,2);
                             if($identificador=='JN'){
                                 $datos["nivel"]="Preescolar";
                             }else if($identificador=='ES' || $identificador=='ST'|| $identificador=='SN'||$identificador=='TV'){
                                  $datos["nivel"]="Secundaria";
                             }else if($identificador=='PR'){
                                 $datos["nivel"]="Primaria";
                             }
                    return $datos; //Retorna un arreglo
            }


        public function get_escuelas_faltantes($region)
        { 
        $this->set_sql("SELECT COUNT(v.cct) as escuelas_faltantes
                        FROM
	                     centros.vista_cct v
                        WHERE
	                    v.subregion = '$region' AND `status` in ('1','4') AND v.categoria_poblacion='1' AND v.turno LIKE '1%' 
	                    AND SUBSTR(cct,3,3) in ('DJN','EJN','DPR','EPR','DES','EES','DST','EST','DSN','DTV')
	                    AND NOT EXISTS ( SELECT * FROM encuesta e W HERE e.cct = v.cct)");

        $rs = mysqli_query($this->db_conn, $this->db_query) or die(mysqli_error($this->db_conn));
        $result = mysqli_fetch_assoc($rs);
        $escuelas_faltantes = $result["escuelas_faltantes"];
        return $escuelas_faltantes;
        }


 
 
    public function insert_encuesta($cct, $turno, $nombre, $municipio, $localidad, $region, $nivel, $zona_escolar, $jefatura, $vandalizado, $vandalizado_si, $atendido,
    $servicio_electrico, $focos, $contactos, $red_electrica_interior, $red_electrica_exterior, $servicio_agua, $red_agua, $servicio_drenaje, $red_drenaje, $fosa_septica, 
    $inodoro, $mingitorios, $lavamanos, $puertas, $chapas, $vidrios, $protecciones, $maleza, $basura, $escombro) 
        { 
        //prevent sql inyection 
                $cct= $this->db_conn->real_escape_string($cct); 
                $turno= $this->db_conn->real_escape_string($turno); 
                $nombre= $this->db_conn->real_escape_string($nombre); 
                $municipio= $this->db_conn->real_escape_string($municipio); 
                $localidad = $this->db_conn->real_escape_string($localidad);
                 $region = $this->db_conn->real_escape_string($region);
                $nivel = $this->db_conn->real_escape_string($nivel); 
                $zona_escolar = $this->db_conn->real_escape_string($zona_escolar); 
                $jefatura = $this->db_conn->real_escape_string($jefatura); 

                $vandalizado = $this->db_conn->real_escape_string($vandalizado); 
                $vandalizado_si = $this->db_conn->real_escape_string($vandalizado_si); 
                $atendido = $this->db_conn->real_escape_string($atendido); 
                $servicio_electrico = $this->db_conn->real_escape_string($servicio_electrico);
                $focos = $this->db_conn->real_escape_string($focos);
                $contactos= $this->db_conn->real_escape_string($contactos);
                $red_electrica_interior = $this->db_conn->real_escape_string($red_electrica_interior); 
                $red_electrica_exterior= $this->db_conn->real_escape_string($red_electrica_exterior);
                $servicio_agua= $this->db_conn->real_escape_string($servicio_agua); 
                $servicio_drenaje= $this->db_conn->real_escape_string($servicio_drenaje);
                $red_drenaje= $this->db_conn->real_escape_string($red_drenaje);
                $fosa_septica= $this->db_conn->real_escape_string($fosa_septica);
                $inodoro= $this->db_conn->real_escape_string($inodoro);
                $mingitorios= $this->db_conn->real_escape_string($mingitorios);
                $lavamanos = $this->db_conn->real_escape_string($lavamanos);
                $puertas = $this->db_conn->real_escape_string($puertas);
                $chapas = $this->db_conn->real_escape_string($chapas);
                $vidrios = $this->db_conn->real_escape_string($vidrios);
                $protecciones = $this->db_conn->real_escape_string($protecciones); 
                $maleza = $this->db_conn->real_escape_string($maleza); 
                $basura = $this->db_conn->real_escape_string($basura);
                $escombro = $this->db_conn->real_escape_string($escombro); 
           
                try { 
            
                    $sql = "INSERT INTO encuesta (cct, turno, nombre, municipio, localidad, region, nivel, zona_escolar, jefatura, vandalizado, vandalizado_si, atendido,
                            servicio_electrico, focos, contactos, red_electrica_interior, red_electrica_exterior, servicio_agua, red_agua, servicio_drenaje, red_drenaje, fosa_septica, 
                            inodoro, mingitorios, lavamanos, puertas, chapas, vidrios, protecciones, maleza, basura, escombro) 
                    VALUES('$cct', '$turno', '$nombre', '$municipio', '$localidad', '$region', '$nivel', '$zona_escolar', '$jefatura', '$vandalizado', '$vandalizado_si', '$atendido',
                         '$servicio_electrico', '$focos', '$contactos', '$red_electrica_interior', '$red_electrica_exterior', '$servicio_agua', '$red_agua', 
                         '$servicio_drenaje', '$red_drenaje', '$fosa_septica', '$inodoro', '$mingitorios', '$lavamanos', '$puertas', '$chapas', '$vidrios', '$protecciones', '$maleza',
                          '$basura', '$escombro') "; 
            
                    $this->set_sql($sql); 
                    mysqli_query($this->db_conn, $this->db_query) or die(mysqli_error($this->db_conn)); 
            
                    if (mysqli_affected_rows($this->db_conn) >= 1) { 
                        $inserta = 1; 
                    } else { 
                        $inserta = 0; 
                    } 
                } catch (Exception $e) { 
                    $inserta = 0; 
                } 
                return $inserta; 
        } 




         public function update_encuesta($folio,$cct, $turno, $nombre, $municipio, $localidad, $region, $nivel, $zona_escolar, $jefatura, $vandalizado, $vandalizado_si, $atendido,
    $servicio_electrico, $focos, $contactos, $red_electrica_interior, $red_electrica_exterior, $servicio_agua, $red_agua, $servicio_drenaje, $red_drenaje, $fosa_septica, 
    $inodoro, $mingitorios, $lavamanos, $puertas, $chapas, $vidrios, $protecciones, $maleza, $basura, $escombro) 
    { 
            //prevent sql inyection 
                $folio= $this->db_conn->real_escape_string($folio); 
                $cct= $this->db_conn->real_escape_string($cct); 
                $turno= $this->db_conn->real_escape_string($turno); 
                $nombre= $this->db_conn->real_escape_string($nombre); 
                $municipio= $this->db_conn->real_escape_string($municipio); 
                $localidad = $this->db_conn->real_escape_string($localidad);
                 $region = $this->db_conn->real_escape_string($region);
                $nivel = $this->db_conn->real_escape_string($nivel); 
                $zona_escolar = $this->db_conn->real_escape_string($zona_escolar); 
                $jefatura = $this->db_conn->real_escape_string($jefatura); 

                $vandalizado = $this->db_conn->real_escape_string($vandalizado); 
                $vandalizado_si = $this->db_conn->real_escape_string($vandalizado_si); 
                $atendido = $this->db_conn->real_escape_string($atendido); 
                $servicio_electrico = $this->db_conn->real_escape_string($servicio_electrico);
                $focos = $this->db_conn->real_escape_string($focos);
                $contactos= $this->db_conn->real_escape_string($contactos);
                $red_electrica_interior = $this->db_conn->real_escape_string($red_electrica_interior); 
                $red_electrica_exterior= $this->db_conn->real_escape_string($red_electrica_exterior);
                $servicio_agua= $this->db_conn->real_escape_string($servicio_agua); 
                $servicio_drenaje= $this->db_conn->real_escape_string($servicio_drenaje);
                $red_drenaje= $this->db_conn->real_escape_string($red_drenaje);
                $fosa_septica= $this->db_conn->real_escape_string($fosa_septica);
                $inodoro= $this->db_conn->real_escape_string($inodoro);
                $mingitorios= $this->db_conn->real_escape_string($mingitorios);
                $lavamanos = $this->db_conn->real_escape_string($lavamanos);
                $puertas = $this->db_conn->real_escape_string($puertas);
                $chapas = $this->db_conn->real_escape_string($chapas);
                $vidrios = $this->db_conn->real_escape_string($vidrios);
                $protecciones = $this->db_conn->real_escape_string($protecciones); 
                $maleza = $this->db_conn->real_escape_string($maleza); 
                $basura = $this->db_conn->real_escape_string($basura);
                $escombro = $this->db_conn->real_escape_string($escombro);  
 
        try { 
                    $sql = "UPDATE encuesta
                    set folio='$folio',
                    cct = '$cct', 
                    turno = '$turno',
                    nombre = '$nombre', 
                    municipio = '$municipio', 
                    localidad = '$localidad', 
                    region = '$region',
                    nivel = '$nivel',
                    zona_escolar = '$zona_escolar', 
                    jefatura = '$jefatura', 

                    vandalizado = '$vandalizado', 
                    vandalizado = '$vandalizado',
                    atendido = '$atendido',
                    servicio_electrico= '$servicio_electrico',
                    focos= '$focos',
                    contactos= '$contactos',
                    red_electrica_interior= '$red_electrica_interior',
                    red_electrica_exterior = '$red_electrica_exterior', 
                    servicio_agua = '$servicio_agua',
                    servicio_drenaje = '$servicio_drenaje',
                    red_drenaje = '$red_drenaje', 
                    fosa_septica = '$fosa_septica', 
                    inodoro = '$inodoro', 
                    mingitorios = '$mingitorios', 
                    lavamanos = '$lavamanos', 
                    puertas = '$puertas', 
                    chapas = '$chapas', 
                    vidrios = '$vidrios', 
                    protecciones = '$protecciones',
                    maleza = '$maleza',
                    basura = '$basura' ,
                    escombro = '$escombro'  

                    where folio='$folio' and cct= '$cct'"; 
                    $this->set_sql($sql); 
                    mysqli_query($this->db_conn, $this->db_query) or die(mysqli_error($this->db_conn)); 
 
                    if (mysqli_affected_rows($this->db_conn) >= 1) { 
                        $result = 1; 
                    } else { 
                        $result = 0; 
                    } 
        } catch (Exception $e) { 
                    $result = 0; 
        } 
        return $result; 
    } 




}