<?php
{
    class class_db_beca
    {
        public $db_conn;
        public $db_name;
        public $db_query;
        //Base de Datos
        public function __construct()
        {

            $this->set_db("172.20.30.251", "escuelas_usr", "3ncu3sta", "encuesta_escuelas");//original

            //$this->set_db("localhost","root", "", "atajos");//LOCAL

        }

        public function __destruct()
        {
            $this->close_db();
        }

        public function set_db($host, $user, $passwd, $db)
        {
            if (!isset($this->db_conn)) {
                $this->db_conn = mysqli_connect($host, $user, $passwd, $db);
                $this->db_name = $db;
            }
        }

        public function close_db()
        {
            if (isset($this->db_conn)) {
                mysqli_kill($this->db_conn,$this->db_conn->thread_id);
                mysqli_close($this->db_conn);
            }
        }

        public function set_sql($sql)
        {
            $this->db_query = $sql;
        }

        public function throw_ex($error){
            throw new Exception($error);
        }
    }
}