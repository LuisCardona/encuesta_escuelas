<?php
class encuesta {
    
    //DATOS DE LA ESCUELA
    protected $folio;
    protected $cct;
    protected $turno;
    protected $nombre;
    protected $municipio;
    protected $localidad;
    protected $region;
    protected $nivel;
    protected $zona_escolar;
    protected $jefatura;

    //DATOS DE LA ENCUESTA
    protected $vandalizado;
    protected $vandalizado_si;
    protected $atendido;
    protected $servicio_electrico;
    protected $focos;
    protected $contactos;
    protected $red_electrica_interior;
    protected $red_electrica_exterior;
    protected $servicio_agua;
    protected $red_agua;
    protected $servicio_drenaje;
    protected $red_drenaje;
    protected $fosa_septica;
    protected $inodoro;
    protected $mingitorios;
    protected $lavamanos;
    protected $puertas;
    protected $chapas;
    protected $vidrios;
    protected $protecciones;
    protected $maleza;
    protected $basura;
    protected $escombro;
    
    
    

    public function setfolio($folio) { $this->folio = $folio;}
    public function getfolio() { return $this->folio;}

    public function setcct($cct) { $this->cct = $cct;}
    public function getcct() { return $this->cct;}

    public function setturno($turno) { $this->turno = $turno;}
    public function getturno() { return $this->turno;}

    public function setnombre($nombre) { $this->nombre = $nombre;}
    public function getnombre() { return $this->nombre;}

    public function setmunicipio($municipio) { $this->municipio = $municipio;}
    public function getmunicipio() { return $this->municipio;}
   

    public function setlocalidad($localidad) { $this->localidad = $localidad;}
    public function getlocalidad() { return $this->localidad;}

    public function setregion($region) { $this->region = $region;}
    public function getregion() { return $this->region;}

    public function setnivel($nivel) { $this->nivel = $nivel;}
    public function getnivel() { return $this->nivel;}

    public function setzona_escolar($zona_escolar) { $this->zona_escolar = $zona_escolar;}
    public function getzona_escolar() { return $this->zona_escolar;}

    public function setjefatura($jefatura) { $this->jefatura = $jefatura;}
    public function getjefatura() { return $this->jefatura;}

    public function setvandalizado($vandalizado) { $this->vandalizado = $vandalizado;}
    public function getvandalizado() { return $this->vandalizado;}

    public function setvandalizado_si($vandalizado_si) { $this->vandalizado_si = $vandalizado_si;}
    public function getvandalizado_si() { return $this->vandalizado_si;}

    public function setatendido($atendido) { $this->atendido = $atendido;}
    public function getatendido() { return $this->atendido;}

    public function setservicio_electrico($servicio_electrico) { $this->servicio_electrico = $servicio_electrico;}
    public function getservicio_electrico() { return $this->servicio_electrico;}

    public function setfocos($focos) { $this->focos = $focos;}
    public function getfocos() { return $this->focos;}

    public function setcontactos($contactos) { $this->contactos = $contactos;}
    public function getcontactos() { return $this->contactos;}

    public function setred_electrica_interior($red_electrica_interior) { $this->red_electrica_interior = $red_electrica_interior;}
    public function getred_electrica_interior() { return $this->red_electrica_interior;}

    public function setred_electrica_exterior($red_electrica_exterior) { $this->red_electrica_exterior = $red_electrica_exterior;}
    public function getred_electrica_exterior() { return $this->red_electrica_exterior;}

    public function setservicio_agua($servicio_agua) { $this->servicio_agua = $servicio_agua;}
    public function getservicio_agua() { return $this->servicio_agua;}

    public function setred_agua($red_agua) { $this->red_agua = $red_agua;}
    public function getred_agua() { return $this->red_agua;}

    public function setservicio_drenaje($servicio_drenaje) { $this->servicio_drenaje = $servicio_drenaje;}
    public function getservicio_drenaje() { return $this->servicio_drenaje;}

    public function setred_drenaje($red_drenaje) { $this->red_drenaje = $red_drenaje;}
    public function getred_drenaje() { return $this->red_drenaje;}

    public function setfosa_septica($fosa_septica) { $this->fosa_septica = $fosa_septica;}
    public function getfosa_septica() { return $this->fosa_septica;}

    public function setinodoro($inodoro) { $this->inodoro = $inodoro;}
    public function getinodoro() { return $this->inodoro;}

    public function setmingitorios($mingitorios) { $this->mingitorios = $mingitorios;}
    public function getmingitorios() { return $this->mingitorios;}

    public function setlavamanos($lavamanos) { $this->lavamanos = $lavamanos;}
    public function getlavamanos() { return $this->lavamanos;}

    public function setpuertas($puertas) { $this->puertas = $puertas;}
    public function getpuertas() { return $this->puertas;}

    public function setchapas($chapas) { $this->chapas = $chapas;}
    public function getchapas() { return $this->chapas;}

    public function setvidrios($vidrios) { $this->vidrios = $vidrios;}
    public function getvidrios() { return $this->vidrios;}

    public function setprotecciones($protecciones) { $this->protecciones = $protecciones;}
    public function getprotecciones() { return $this->protecciones;}

    public function setmaleza($maleza) { $this->maleza = $maleza;}
    public function getmaleza() { return $this->maleza;}

    public function setbasura($basura) { $this->basura = $basura;}
    public function getbasura() { return $this->basura;}

    public function setescombro($escombro) { $this->escombro = $escombro;}
    public function getescombro() { return $this->escombro;}
    
    
 }
 ?>